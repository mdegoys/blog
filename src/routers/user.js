import express from 'express'
import passport from 'passport'
import passportLocal from 'passport-local';
const LocalStrategy = passportLocal.Strategy;
import User from './../models/users'
const userRouter = express.Router();
import bCrypt from 'bcrypt-nodejs'
import multer from 'multer'
import cloudinary from 'cloudinary'

// cloudinary setup
cloudinary.config({ 
  cloud_name: 'dyk5dkac5', 
  api_key: '612142695762262', 
  api_secret: 'HiR3VD8kBoSOvUJ3Q8EqfoARKbQ' 
});


// multer setup
const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, './uploads/')
	},
	filename: function (req, file, cb) {
		cb(null, Date.now()+'-'+file.originalname)
	}
})
const upload = multer({ storage })


const createHash = (password) => {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null)
}

const isValidPassword = (user, password) => {
     return bCrypt.compareSync(password, user.password)
}

passport.use(new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password'
	},
  function(email, password, done) {
    User.findOne({ email: email }, function(err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Nom d\'utilisateur incorrect.' });
      }
      if (!isValidPassword(user, password)) {
        return done(null, false, { message: 'Mot de passe invalide.' });
      }
      return done(null, user);
    });
  }
));

userRouter.get('/login', function(req, res) {
		res.render('user_login', { title : "Connexion utilisateur"});
});

userRouter.post('/login', passport.authenticate('local',
	{ successRedirect: '/article',
	  failureRedirect: 'login',
		failureFlash: true })
);

userRouter.get('/add', function(req, res) {
	res.render('user_edit', {title: 'Créer un compte'});
});


userRouter.post('/add', upload.single('img'), async function(req, res) {
	req.body.password = createHash(req.body.password);

//	if (req.file) {
//		cloudinary.v2.uploader.upload(req.file.path,
//    function(error, result){
//			if (result) req.body.avatar = result.secure_url;
//		});
//	}
//	setTimeout(function(){
//		let user = new User(req.body);
//		user.save()
//			.then(user => {
//				req.flash('success', 'Le compte a bien été créé');
//				res.redirect('login');
//			})
//			.catch(err => {
//				req.flash('failure', 'Le compte n\'a pas pu être créé :(');
//				res.redirect('add');
//			});
//	}, 3000);
	const saveUser = () => {
		let user = new User(req.body);
			user.save()
				.then(user => {
					req.flash('success', 'Le compte a bien été créé');
					res.redirect('login');
				})
				.catch(err => {
					req.flash('failure', 'Le compte n\'a pas pu être créé :(');
					res.redirect('add');
				});
	}


	if (req.file) {
		const result = await cloudinary.uploader.upload(req.file.path)
		req.body.avatar = result.secure_url;
		saveUser()
	}
	else {
		saveUser()
	}


});

userRouter.get('/edit', function(req, res) {
	User.findOne({_id:req.user.id}, function (err, result) {
		if (err) throw err
		res.render('user_edit', { title: 'Modifier ses infos', result});
	});
});

userRouter.post('/edit', upload.single('img'), async function(req, res) {
	req.body.password = createHash(req.body.password);
	
	const saveUser = () => {
		User.findOneAndUpdate({_id:req.user.id}, req.body, {new: true, runValidators:true}, function (err, result) {
			if (err) throw err
			req.flash('success', 'Vos infos ont bien été modifiées');
			res.redirect('/user/edit');
		});
	}

	if (req.file) {
		const result = await cloudinary.uploader.upload(req.file.path)
		req.body.avatar = result.secure_url;
		saveUser();
	}
	else {
		saveUser();
	}

});



userRouter.get('/logout', function(req, res){
  req.logout();
	req.flash('success', 'Vous avez bien été déconnecté.');
  res.redirect('/');
});

export default userRouter;
