import express from 'express'
import Comment from './../models/comments'
const commentRouter = express.Router();

// list of user comments, of user comments liked by others, of comments the user liked
commentRouter.get('/', function(req, res) {
	Comment.find({user:req.user._id} , function(err, result) {
		if (err) throw err
		res.render('comment_view_all', { title: 'Mes commentaires', result})	
	}).populate('user').populate('likes');
});

commentRouter.get('/othersliked', function(req, res) {
	Comment.find({ user : req.user._id, likes: { $exists: true, $not: {$size: 0} } }, function(err, result) {
		if (err) throw err
		res.render('comment_view_all', { title : 'Mes commentaires que d\'autres ont liké', result });
	}).populate('user').populate('likes');


});

commentRouter.get('/iliked', function(req, res) {
	Comment.find({ likes : req.user._id }, function(err, result) {
		if (err) throw err
		res.render('comment_view_all', { title : 'Les commentaires que j\'ai liké', result });
	}).populate('user').populate('likes');
});

// get to like a comment
commentRouter.get('/like/:id_comment', function(req, res) {
	Comment.findOneAndUpdate({ _id : req.params.id_comment }, { $addToSet : { likes : req.user._id } }, function(err, result) {
		if (err) throw err
		req.flash('success', 'Commentaire liké !');
		res.redirect(`/article/${result.article}`);
	})
})

// form + post to add a new comment
commentRouter.get('/add/:id_article', function(req, res) {
	res.render('comment_edit', {title : "Ajouter un commentaire"});
});

commentRouter.post('/add/:id_article', function(req, res) {
	req.body.user = req.user._id;
	req.body.article = req.params.id_article;
	let comment = new Comment(req.body);
	comment.save()
		.then(comment => {
			req.flash('success', 'Le commentaire a bien été ajouté');
			res.redirect(`/article/${req.params.id_article}`);
		})
		.catch(err => {
			req.flash('failure', 'Le commentaire n\'a pas pu être ajouté :(');
			res.redirect(`/comment/add/${req.params.id_article}`);
		});
});


// form + post to edit an existing comment
commentRouter.get('/edit/:id_comment', function(req, res) {
	Comment.findOne({_id:req.params.id_comment} , function(err, result) {
		if (err) throw err
		res.render('comment_edit', {title: result.title+ ' - édition du commentaire', result})	
	});
});

commentRouter.post('/edit/:id_comment', function(req, res) {
	Comment.findOneAndUpdate({_id:req.params.id_comment}, req.body, {new: true, runValidators:true} , function(err, result) {
		if (err) throw err
		req.flash('success', 'Le commentaire a bien été édité');
		res.render('comment_edit', {title: result.title+ ' - édition du commentaire', result})	
	});
});

export default commentRouter;
