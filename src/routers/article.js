import express from 'express'
import Article from './../models/articles'
import Comment from './../models/comments'
const articleRouter = express.Router();

articleRouter.get('/', function(req, res) {
	Article.find({ isdraft:false}, function(err, result) {
		if (err) throw err;
		res.render('article_view_all', { title: "Bienvenue sur Mi Simple Blog", result})
	});
});

articleRouter.get('/draft', function(req, res) {
	Article.find({ isdraft: true }, function(err, result) {
		if (err) throw err;
		res.render('article_view_all', { title: "Les brouillons", result})
	});
});

articleRouter.get('/add', function(req, res) {
	res.render('article_edit', {title : "Ajouter un article"});
});

articleRouter.post('/add', function(req, res) {
	req.body.isdraft = Boolean(req.body.isdraft)
	let article = new Article(req.body);
	article.save()
		.then(article => {
			req.flash('success', 'L\'article a bien été créé');
			res.redirect('/article');
		})
		.catch(err => {
			req.flash('failure', 'L\'article n\'a pas pu être créé :(');
			res.redirect('add');
		});
});

articleRouter.get('/:id', function(req, res) {
	Article.findOne({_id:req.params.id} , function(err, result) {
		if (err) throw err
		Comment.find({ article: req.params.id }, function(err_comments, result_comments) {
			if (err_comments) throw err_comments
			res.render('article_view', {title: result.title+ ' - article', result, result_comments})	;
		}).populate('user').populate('likes');
	});
});


articleRouter.get('/edit/:id', function(req, res) {
	Article.findOne({_id:req.params.id}, function (err, result) {
		if (err) throw err
		res.render('article_edit', { title: result.title + ' - édition de l\'article', result});
	});
});

articleRouter.post('/edit/:id', function(req, res) {
	Article.findOneAndUpdate({_id:req.params.id}, req.body, {new: true, runValidators:true}, function (err, result) {
		if (err) throw err
		req.flash('success', 'L\'article a bien été édité.');
		//res.render('article_edit', { title: result.title + ' - édition de l\'article', result});
		res.redirect(`/article/${req.params.id}`);
	});
});


articleRouter.get('/del/:id', function(req, res) {
	Article.remove({_id:req.params.id}, function (err, result) {
		if (err) throw err
		req.flash('success', 'L\'article a bien été supprimé.');
		res.redirect('/article/');
	});
});
		

export default articleRouter;
