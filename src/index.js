import express from 'express'
import volleyball from 'volleyball'
import mongoose from 'mongoose'
import pug from 'pug'
import articleRouter from './routers/article'
import userRouter from './routers/user'
import commentRouter from './routers/comment'
import 'dotenv/config'
import session from 'express-session'
import passport from 'passport'
import flash from 'connect-flash'
import User from './models/users'

const { PORT, DB_HOST, DB_USER, DB_PASS }  = process.env;

mongoose.connect(`mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}`, { useNewUrlParser: true });

const app = express()

app.set('view engine', 'pug');
app.set('views', './views')

app.use('/public', express.static('./public'));
app.use('/uploads', express.static('./uploads'));
app.use(session({ secret: "cats", resave: true,
    saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(flash());

app.use((req, res, next) => {
	res.locals.flashes = req.flash();
	res.locals.user = req.user || null;
	next();
});

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  User.findById(id, function(err, user) {
    cb(err, user);
  });
});

app.get('/', (req, res) => {
	res.redirect('/article');
});

app.use('/article', articleRouter);
app.use('/user', userRouter);
app.use('/comment', commentRouter);

app.listen(PORT, () => {
	console.log(`on écoute le port ${PORT}`);
});
