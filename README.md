CAHIER DES CHARGES - BLOG
Lisez ce document jusqu'au bout et prenez connaissances de toutes les consignes avant de
commencer à travailler sur quoique ce soit ...
CONTEXTE
Vous devez concevoir un blog.
FONCTIONNALITÉS
Fonctionnalités basiques (obligatoire) :
Votre blog doit avoir au moins les fonctionnalités suivantes :
➔ en tant que visiteur, je veux consulter les articles disponibles sur le site
➔ en tant qu'administrateur, je veux pouvoir avoir accès à un compte “Administrateur”
pré-créé.
➔ en tant qu'administrateur, je veux pouvoir me connecter à mon compte (email + mot
de passe), et avoir accès à une session me permettant d’accéder au back-office
➔ en tant qu'administrateur, je veux ajouter/modifier/supprimer un article
➔ en tant qu'administrateur, je veux sauvegarder un article en brouillon sans devoir le
publier
➔ en tant qu'administrateur, je veux publier un article sauvegardé en brouillon
➔ en tant qu'administrateur, je veux consulter la liste des articles non publiés et la liste
des articles publiés
➔ en tant qu'administrateur, je veux ajouter/modifier des informations de mon profil
(nom, prénom, e-mail, mot de passe, avatar)
Ceci constitue les fonctionnalités minimales, si vous pensez à d'autres choses, faites-vous
plaisir. Mais réalisez d'abord les fonctionnalités citées ci-dessus. Et n'hésitez pas à consulter
votre formateur pour avoir du feedback.



Fonctionnalités avancées (optionnelles) :
➔ en tant que visiteur, je veux pouvoir me créer un compte “utilisateur”
➔ en tant qu’utilisateur, je veux ajouter/modifier des informations de mon profil (nom,

1prénom, e-mail, mot de passe, avatar)
➔ en tant qu’utilisateur, je veux pouvoir ajouter/modifier des commentaires sous les
articles postés par l’administrateurs
➔ en tant qu’utilisateur, je veux pouvoir consulter la liste des commentaires que j’ai
postés
➔ en tant qu’utilisateur, je veux pouvoir liker le commentaire d’un autre utilisateur
➔ en tant qu’utilisateur, je veux pouvoir consulter la liste des commentaires d’autres
utilisataures que j’ai likés
➔ en tant qu’utilisateur, je veux pouvoir consulter la liste de mes commentaires qui ont
été likés par d’autres utilisateurs, afin de voir quels utilisateurs ont liké lesquels de
mes commentaires



CONTRAINTES TECHNIQUES
Vous devrez considérer les contraintes techniques suivantes :
➔ Vous ne créerez pas un site vitrine, mais un blog complet.
➔ Vous produirez une interface proposant au moins deux animations
➔ Pour l'aspect back vous créerez un back-office lié à une base de données
Pour le front :
➔ Vous utiliserez HTML, CSS et JavaScript.
➔ Comme indiqué ci-dessus, vous devrez intégrer des interactions et des animations
qui bénéficient à l’UX. N'en faites pas trop, tout est question de dosage.
➔ Vous êtes libre d'utiliser ou non un framework ou une librairie quelconque orientée
front-end.
➔ Votre site doit être responsive. Il doit s'afficher correctement sur des grands et des
petits écrans, sur tablette et sur smartphone. Vous ne devez pas avoir exactement le
même positionnement des éléments en fonction des écrans. Pensez ergonomie et
utilisation pratique.
➔ Votre site doit être au moins compatible avec une version récente de Chrome.
➔ Vous veillerez également à appliquer un minimum de bonnes pratiques pour avoir un
site de qualité : référencement naturel, SEO, accessibilité, etc.
Pour le back :
➔ Vous utiliserez le langage back-end de votre choix (Ruby, PHP, Python, JavaScript,
etc.).

2➔ Vous concevrez une base de données. Vous êtes 100% libre sur le choix de la
technologie, que ce soit une base de données relationnelle ou non-relationnelle.
➔ Vous pouvez utiliser un framework pour réaliser votre livrable, mais ce n’est pas
obligatoire. NB : l’utilisation de CMS comme Wordpress ou Drupal est proscrite.